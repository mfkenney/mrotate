// Package mrotate provides 3-d rotation matricies
package mrotate

import "math"

type Axis int

const (
	Xaxis Axis = iota
	Yaxis
	Zaxis
)

type Matrix [3][3]float64
type Vector [3]float64

// NewMatrix returns a rotation matrix for the specified angle (in degrees) about
// the axis ax.
func NewMatrix(ax Axis, angle float64) Matrix {
	var m Matrix
	var s, c float64

	switch angle {
	case 0:
		s = 0
		c = 1
	case 90:
		s = 1
		c = 0
	case 180:
		s = 0
		c = -1
	case 270:
		s = -1
		c = 0
	default:
		theta := angle * math.Pi / 180.
		s = math.Sin(theta)
		c = math.Cos(theta)
	}

	switch ax {
	case Xaxis:
		m[0][0] = 1
		m[1][1] = c
		m[1][2] = -s
		m[2][1] = s
		m[2][2] = c
	case Yaxis:
		m[0][0] = c
		m[0][2] = s
		m[1][1] = 1
		m[2][0] = -s
		m[2][2] = c
	case Zaxis:
		m[0][0] = c
		m[0][1] = -s
		m[1][0] = s
		m[1][1] = c
		m[2][2] = 1
	}
	return m
}

// Column extracts the column j vector from a matrix.
func (m Matrix) Column(j int) Vector {
	var x Vector
	for i := 0; i < 3; i++ {
		x[i] = m[i][j]
	}

	return x
}

// PreMult returns the matrix product a*m
func (m Matrix) PreMult(a Matrix) Matrix {
	var b Matrix
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			b[i][j] = a.Column(j).DotProd(Vector(m[i]))
		}
	}

	return b
}

func (v Vector) DotProd(x Vector) float64 {
	var sum float64
	for i := 0; i < 3; i++ {
		sum += (v[i] * x[i])
	}
	return sum
}

// Mult returns the product m*v
func (v Vector) Mult(m Matrix) Vector {
	var x Vector

	for i := 0; i < 3; i++ {
		x[i] = v.DotProd(Vector(m[i]))
	}

	return x
}
