package mrotate

import "testing"

func TestRotation(t *testing.T) {
	table := []struct {
		r       Matrix
		in, out Vector
	}{
		{
			r:   NewMatrix(Zaxis, 90),
			in:  Vector{1, 0, 0},
			out: Vector{0, 1, 0},
		},
		{
			r:   NewMatrix(Xaxis, 90),
			in:  Vector{0, 1, 0},
			out: Vector{0, 0, 1},
		},
		{
			r:   NewMatrix(Yaxis, 90),
			in:  Vector{0, 0, 1},
			out: Vector{1, 0, 0},
		},
	}

	for _, e := range table {
		out := e.in.Mult(e.r)
		if out != e.out {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, out)
		}
	}
}
